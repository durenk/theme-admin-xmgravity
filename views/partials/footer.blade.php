<footer class="sidebar__footer">
	<a class="tip" href="{{ URL::to('http://www.cartalyst.com/licence') }}" target="_blank" Title="&copy; Cartalyst">
		<img src="{{ Asset::getUrl('img/licence.png') }}" alt="">
	</a>
</footer>
